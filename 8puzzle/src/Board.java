import java.util.ArrayList;

public class Board {

    private int N;

    private int[] board;

    // construct a board from an N-by-N array of blocks
    // (where blocks[i][j] = block in row i, column j)
    public Board(int[][] blocks) {
        N = blocks.length;
        board = new int[N * N];
        for (int row = 0; row < N; row++)
            for (int col = 0; col < N; col++)
                board[row * N + col] = blocks[row][col];
    }

    // board dimension N
    public int dimension() {
        return N;
    }

    // number of blocks out of place
    public int hamming() {
        int distance = 0;
        for (int i = 0; i < N * N; i++)
            if (board[i] != 0 && (board[i] - i - 1) != 0)
                distance += 1;
        return distance;
    }

    // sum of Manhattan distances between blocks and goal
    public int manhattan() {
        int distance = 0;
        for (int i = 0; i < N * N; i++) {
            if (board[i] != 0) {
                int correctRow = (board[i] - 1) / N;
                int correctCol = (board[i] - 1) % N;
                int currentRow = i / N;
                int currentCol = i % N;
                distance += Math.abs(correctRow - currentRow)
                        + Math.abs(correctCol - currentCol);
            }
        }
        return distance;
    }

    // is this board the goal board?
    public boolean isGoal() {
        return hamming() == 0;
    }

    // a board that is obtained by exchanging two adjacent blocks in the same
    // row
    public Board twin() {
        int[][] twin = new int[N][N];
        for (int row = 0; row < N; row++)
            for (int col = 0; col < N; col++)
                twin[row][col] = board[row * N + col];

        for (int row = 0; row < N; row++)
            for (int col = 0; col < N - 1; col++)
                if (twin[row][col] != 0 && twin[row][col + 1] != 0) {
                    int tmp = twin[row][col];
                    twin[row][col] = twin[row][col + 1];
                    twin[row][col + 1] = tmp;
                    return new Board(twin);
                }
        return new Board(twin);
    }

    // does this board equal y?
    public boolean equals(Object y) {
        if (y == this)
            return true;
        if (y == null)
            return false;
        if (y.getClass() != this.getClass())
            return false;
        Board that = (Board) y;
        for (int i = 0; i < N * N; i++)
            if (this.board[i] != that.board[i])
                return false;
        return true;
    }

    // swaps [rowIndex][colIndex] with [rowIndex][colIndex - 1]
    private Board shiftLeft(int rowIndex, int colIndex) {
        assert (colIndex > 0);
        int[][] out = new int[N][N];
        for (int row = 0; row < N; row++)
            for (int col = 0; col < N; col++)
                out[row][col] = board[row * N + col];
        int tmp = out[rowIndex][colIndex - 1];
        out[rowIndex][colIndex - 1] = out[rowIndex][colIndex];
        out[rowIndex][colIndex] = tmp;
        return new Board(out);
    }

    // swaps [rowIndex][colIndex] with [rowIndex][colIndex + 1]
    private Board shiftRight(int rowIndex, int colIndex) {
        assert (colIndex < N - 1);
        int[][] out = new int[N][N];
        for (int row = 0; row < N; row++)
            for (int col = 0; col < N; col++)
                out[row][col] = board[row * N + col];
        int tmp = out[rowIndex][colIndex + 1];
        out[rowIndex][colIndex + 1] = out[rowIndex][colIndex];
        out[rowIndex][colIndex] = tmp;
        return new Board(out);
    }

    // swaps [rowIndex][colIndex] with [rowIndex - 1][colIndex]
    private Board shiftUp(int rowIndex, int colIndex) {
        assert (rowIndex > 0);
        int[][] out = new int[N][N];
        for (int row = 0; row < N; row++)
            for (int col = 0; col < N; col++)
                out[row][col] = board[row * N + col];
        int tmp = out[rowIndex - 1][colIndex];
        out[rowIndex - 1][colIndex] = out[rowIndex][colIndex];
        out[rowIndex][colIndex] = tmp;
        return new Board(out);
    }

    // swaps [rowIndex][colIndex] with [rowIndex + 1][colIndex]
    private Board shiftDown(int rowIndex, int colIndex) {
        assert (rowIndex < N - 1);
        int[][] out = new int[N][N];
        for (int row = 0; row < N; row++)
            for (int col = 0; col < N; col++)
                out[row][col] = board[row * N + col];
        int tmp = out[rowIndex + 1][colIndex];
        out[rowIndex + 1][colIndex] = out[rowIndex][colIndex];
        out[rowIndex][colIndex] = tmp;
        return new Board(out);
    }

    // all neighboring boards
    public Iterable<Board> neighbors() {
        ArrayList<Board> neighbors = new ArrayList<Board>();
        int zeroRowIndex = -1;
        int zeroColIndex = -1;
        for (int i = 0; i < N * N; i++)
            if (board[i] == 0) {
                zeroRowIndex = i / N;
                zeroColIndex = i % N;
                break;
            }
        if (zeroRowIndex > 0)
            neighbors.add(shiftUp(zeroRowIndex, zeroColIndex));
        if (zeroColIndex > 0)
            neighbors.add(shiftLeft(zeroRowIndex, zeroColIndex));
        if (zeroRowIndex < N - 1)
            neighbors.add(shiftDown(zeroRowIndex, zeroColIndex));
        if (zeroColIndex < N - 1)
            neighbors.add(shiftRight(zeroRowIndex, zeroColIndex));
        return neighbors;
    }

    // string representation of this board (in the output format specified
    // below)
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(N + "\n");
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                s.append(String.format("%2d ", board[i * N + j]));
            }
            s.append("\n");
        }
        return s.toString();
    }

    // unit tests (not graded)
    public static void main(String[] args) {

    }
}
