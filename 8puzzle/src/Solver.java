import java.util.ArrayList;
import java.util.Comparator;

public class Solver {
    // find a solution to the initial board (using the A* algorithm)

    private ArrayList<Board> moves;
    private boolean isSolvable = false;
    private int totalMoves = 0;

    private class BoardMove {

        private Board parent;
        private Board board;
        private int moveNumber;

        public BoardMove(Board b, int k, Board p) {
            board = b;
            moveNumber = k;
            parent = p;
        }

        public int hamming() {
            return board.hamming();
        }

        public int priority() {
            return (board.manhattan() + moveNumber);
        }
    }

    private class BoardComparator implements Comparator<BoardMove> {
        @Override
        public int compare(BoardMove arg0, BoardMove arg1) {
            int p0 = arg0.priority();
            int p1 = arg1.priority();
            if (p0 < p1)
                return -1;
            if (p0 > p1)
                return 1;
            if (p0 == p1) {
                int h0 = arg0.hamming();
                int h1 = arg1.hamming();
                if (h0 < h1)
                    return -1;
                if (h0 > h1)
                    return 1;
            }
            return 0;
        }
    }

    public Solver(Board initial) {
        moves = new ArrayList<Board>();

        MinPQ<BoardMove> pq = new MinPQ<BoardMove>(new BoardComparator());
        pq.insert(new BoardMove(initial, 0, null));
        BoardMove move = pq.delMin();
        moves.add(move.board);

        MinPQ<BoardMove> twinpq = new MinPQ<BoardMove>(new BoardComparator());
        twinpq.insert(new BoardMove(initial.twin(), 0, null));
        BoardMove twinmove = twinpq.delMin();

        while (!move.board.isGoal() && !twinmove.board.isGoal()) {
            for (Board child : move.board.neighbors()) {
                if (!child.equals(move.parent)) {
                    pq.insert(new BoardMove(child, move.moveNumber + 1,
                            move.board));
                }
            }
            move = pq.delMin();
            moves.add(move.board);

            for (Board twinchild : twinmove.board.neighbors()) {
                if (!twinchild.equals(twinmove.parent)) {
                    twinpq.insert(new BoardMove(twinchild,
                            twinmove.moveNumber + 1, twinmove.board));
                }
            }
            twinmove = twinpq.delMin();

        }

        isSolvable = move.board.isGoal();
        if (isSolvable)
            totalMoves = move.moveNumber;
    }

    // is the initial board solvable?
    public boolean isSolvable() {
        return isSolvable;
    }

    // min number of moves to solve initial board; -1 if unsolvable
    public int moves() {
        return totalMoves;
    }

    // sequence of boards in a shortest solution; null if unsolvable
    public Iterable<Board> solution() {
        return moves;
    }

    // solve a slider puzzle (given below)
    public static void main(String[] args) {
        // create initial board from file
        In in = new In(args[0]);
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);

        // solve the puzzle
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }
}