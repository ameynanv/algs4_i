import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {
    private Item[] q; // queue elements
    private int N = 0; // number of elements on queue

    // construct an empty randomized queue
    public RandomizedQueue() {
        q = (Item[]) new Object[2];
    }

    // is the queue empty?
    public boolean isEmpty() {
        return N == 0;
    }

    // return the number of items on the queue
    public int size() {
        return N;
    }

    // resize the underlying array
    private void resize(int max) {
        assert max >= N;
        Item[] temp = (Item[]) new Object[max];
        for (int i = 0; i < N; i++) {
            temp[i] = q[i];
        }
        q = temp;
    }

    // add the item
    public void enqueue(Item item) {
        if (item == null)
            throw new NullPointerException("Null data is being added");
        // double size of array if necessary and recopy to front of array
        if (N == q.length)
            resize(2 * q.length); // double size of array if necessary
        q[N++] = item; // add item
    }

    // remove and return a random item
    public Item dequeue() {
        if (isEmpty())
            throw new NoSuchElementException("Queue underflow");
        int indx = StdRandom.uniform(N);
        Item temp = q[indx];
        q[indx] = q[N-1];
        q[N-1] = temp;

        Item item = q[N-1];
        q[N-1] = null; // to avoid loitering
        N--;

        // shrink size of array if necessary
        if (N > 0 && N == q.length / 4)
            resize(q.length / 2);
        return item;
    }

    // return (but do not remove) a random item
    public Item sample() {
        if (isEmpty())
            throw new NoSuchElementException("Queue underflow");
        int indx = StdRandom.uniform(N);
        return q[indx];
    }

    // an iterator, doesn't implement remove() since it's optional
    private class ArrayIterator implements Iterator<Item> {
        private int i = 0;
        private int[] indexArray;

        private ArrayIterator() {
            indexArray = new int[N];
            for (int k = 0; k < N; k++) {
                indexArray[k] = k;
            }
            StdRandom.shuffle(indexArray);
        }
        
        
        public boolean hasNext() {
            return i < N;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public Item next() {
            if (!hasNext())
                throw new NoSuchElementException();
            Item result = q[indexArray[i++]];
            return result;
        }
    }

    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
        return new ArrayIterator();
    }

    
    private void print() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (Item item : this) {
            builder.append(",");
            builder.append(item);
        }
        builder.append("]");
        String result = builder.toString().replaceFirst(",", "");
        System.out.println("SZ = " + this.size() + ", Queue= " + result);
    }
    
    // unit testing
    public static void main(String[] args) {
        RandomizedQueue<Integer> rq = new RandomizedQueue<Integer>();
        rq.enqueue(1);
        rq.enqueue(2);
        rq.enqueue(3);
        rq.enqueue(4);
        rq.enqueue(5);
        System.out.println(rq.dequeue());
        System.out.println(rq.dequeue());
        System.out.println(rq.dequeue());
        System.out.println(rq.dequeue());
        System.out.println(rq.dequeue());
        rq.enqueue(1);
        rq.enqueue(2);
        rq.enqueue(3);
        rq.enqueue(4);
        rq.enqueue(5);
        System.out.println(rq.dequeue());
        System.out.println(rq.dequeue());
        System.out.println(rq.dequeue());
        System.out.println(rq.dequeue());
        System.out.println(rq.dequeue());
        rq.print();
    }
}
