import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {

    private int N;
    private Node<Item> first;
    private Node<Item> last;

    private static class Node<Item> {
        private Item data;
        private Node<Item> next;
        private Node<Item> prev;
    }

    private class ListIterator implements Iterator<Item> {

        private Node<Item> current = first;

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public Item next() {
            if (!hasNext()) 
                throw new NoSuchElementException("Collection is empty");
            Item result = current.data;
            current = current.next;
            return result;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException(
                    "Remove method is not supported");
        }
    }

    // construct an empty deque
    public Deque() {
        N = 0;
        first = null;
        last = first;
    }

    // is the deque empty?
    public boolean isEmpty() {
        return N == 0;
    }

    // return the number of items on the deque
    public int size() {
        return N;
    }

    // add the item to the front
    public void addFirst(Item item) {
        if (item == null)
            throw new NullPointerException("Null data is being added");
        if (first == null) {
            first = new Node<Item>();
            first.data = item;
            first.next = null;
            first.prev = null;
            last = first;
        } else {
            Node<Item> oldFirst = first;
            first = new Node<Item>();
            first.data = item;
            first.next = oldFirst;
            first.prev = null;
            oldFirst.prev = first;
        }
        N++;
    }

    // add the item to the end
    public void addLast(Item item) {
        if (item == null)
            throw new NullPointerException("Null data is being added");
        if (first == null) {
            addFirst(item);
        } else {
            Node<Item> oldLast = last;
            last = new Node<Item>();
            last.data = item;
            last.prev = oldLast;
            last.next = null;
            oldLast.next = last;
            N++;
        }
    }

    // remove and return the item from the front
    public Item removeFirst() {
        if (N == 0)
            throw new NoSuchElementException("Deque is empty");
        Item result = first.data;
        first = first.next;
        if (first != null)
            first.prev = null;
        else
            last = null;
        N--;
        return result;
    }

    // remove and return the item from the end
    public Item removeLast() {
        if (N == 0)
            throw new NoSuchElementException("Deque is empty");
        Item result = last.data;
        last = last.prev;
        if (last != null)
            last.next = null;
        else
            first = null;
        N--;
        return result;
    }

    // return an iterator over items in order from front to end
    public Iterator<Item> iterator() {
        return new ListIterator();
    }

    private void print() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (Item item : this) {
            builder.append(",");
            builder.append(item);
        }
        builder.append("]");
        String result = builder.toString().replaceFirst(",", "");
        System.out.println("SZ = " + this.size() + ", Deque= " + result);
    }

    // unit testing
    public static void main(String[] args) {
        Deque<Integer> dq = new Deque<>();
        dq.addFirst(1);
        dq.addLast(2);
        dq.addLast(3);
        System.out.println(dq.removeFirst());
        System.out.println(dq.removeLast());
        System.out.println(dq.removeLast());
        dq.print();
        dq.addFirst(1);
        dq.print();

    }
}
