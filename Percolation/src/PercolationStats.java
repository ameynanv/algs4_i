public class PercolationStats {
    private double[] percolationRuns;
    private int T;

    // perform T independent experiments on an N-by-N grid
    public PercolationStats(int N, int T) {
        if (N <= 0 || T <= 0)
            throw new IllegalArgumentException(" N >=0 && T>= 0");
        this.T = T;
        percolationRuns = new double[T];

        for (int i = 0; i < T; i++) {
            Percolation p = new Percolation(N);
            int cnt = 0;
            while (!p.percolates()) {
                cnt++;
                int index = StdRandom.uniform(0, N * N);
                int row = index / N + 1;
                int col = index - (index / N * N) + 1;
                while (p.isOpen(row, col)) {
                    index = StdRandom.uniform(0, N * N);
                    row = index / N + 1;
                    col = index - (index / N * N) + 1;
                }
                p.open(row, col);

            }
            percolationRuns[i] = ((cnt * 1.0) / (N * N * 1.0));
        }
    }

    // sample mean of percolation threshold
    public double mean() {
        return StdStats.mean(percolationRuns);
    }

    // sample standard deviation of percolation threshold
    public double stddev() {
        return StdStats.stddev(percolationRuns);
    }

    // low endpoint of 95% confidence interval
    public double confidenceLo() {
        double mean = mean();
        double stddev = stddev();
        return (mean - 1.96 * stddev / Math.sqrt(T));
    }

    // high endpoint of 95% confidence interval
    public double confidenceHi() {
        double mean = mean();
        double stddev = stddev();
        return (mean + 1.96 * stddev / Math.sqrt(T));
    }

    // test client (described below)
    public static void main(String[] args) {
        PercolationStats stats = new PercolationStats(100, 50);
        System.out.println("mean                    = " + stats.mean());
        System.out.println("stddev                  = " + stats.stddev());
        System.out.println("95% confidence interval = " + stats.confidenceLo()
                + " , " + stats.confidenceHi());
    }
}