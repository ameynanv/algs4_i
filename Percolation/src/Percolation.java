public class Percolation {

    private int N;
    private WeightedQuickUnionUF grid;
    private WeightedQuickUnionUF grid2;
    private boolean[] gridStatus;

    // create N-by-N grid, with all sites blocked
    public Percolation(int N) {
        if (N <= 0)
            throw new IllegalArgumentException(
                    "Size of Percolation has to be greater than 0");
        this.N = N;
        grid = new WeightedQuickUnionUF(N * N + 2);
        grid2 = new WeightedQuickUnionUF(N * N + 1);
        gridStatus = new boolean[N * N];
        for (int i = 0; i < gridStatus.length; i++) {
            gridStatus[i] = false;
        }
        for (int i = 1; i < N + 1; i++) {
            // connect Pseduo Node 0 to all the nodes of the first row.
            grid.union(0, gridIndex(1, i));
            grid2.union(0, gridIndex(1, i));

            // connect Pseduo Node N^2+N+1 to all
            // the nodes of the last two rows.
            grid.union(gridIndex(N, i), N * N + 1);
        }
    }

    private int gridStatusIndex(int row, int col) {
        return ((row - 1) * N + (col - 1));
    }

    private int gridIndex(int row, int col) {
        return (1 + (row - 1) * N + (col - 1));
    }

    private boolean inLimit(int row, int col) {
        return (row > 0 && col > 0 && row < N + 1 && col < N + 1);
    }

    // open site (row i, column j) if it is not open already
    public void open(int i, int j) {
        if (!inLimit(i, j))
            throw new IndexOutOfBoundsException("(i,j)=(" + i + "," + j
                    + ") Should be within (1,1) to (" + N + "," + N + ")");
        if (!isOpen(i, j)) {
            gridStatus[gridStatusIndex(i, j)] = true;
            if (inLimit(i - 1, j) && isOpen(i - 1, j)) {
                grid.union(gridIndex(i - 1, j), gridIndex(i, j));
                grid2.union(gridIndex(i - 1, j), gridIndex(i, j));
            }
            if (inLimit(i + 1, j) && isOpen(i + 1, j)) {
                grid.union(gridIndex(i + 1, j), gridIndex(i, j));
                grid2.union(gridIndex(i + 1, j), gridIndex(i, j));
            }
            if (inLimit(i, j - 1) && isOpen(i, j - 1)) {
                grid.union(gridIndex(i, j - 1), gridIndex(i, j));
                grid2.union(gridIndex(i, j - 1), gridIndex(i, j));
            }
            if (inLimit(i, j + 1) && isOpen(i, j + 1)) {
                grid.union(gridIndex(i, j + 1), gridIndex(i, j));
                grid2.union(gridIndex(i, j + 1), gridIndex(i, j));
            }
        }
    }

    // is site (row i, column j) open?
    public boolean isOpen(int i, int j) {
        if (!inLimit(i, j))
            throw new IndexOutOfBoundsException("(i,j)=(" + i + "," + j
                    + ") Should be within (1,1) to (" + N + "," + N + ")");
        return gridStatus[gridStatusIndex(i, j)];
    }

    // is site (row i, column j) full?
    public boolean isFull(int i, int j) {
        if (!inLimit(i, j))
            throw new IndexOutOfBoundsException("(i,j)=(" + i + "," + j
                    + ") Should be within (1,1) to (" + N + "," + N + ")");
        return (isOpen(i, j) && grid2.connected(gridIndex(i, j), 0));
    }

    // does the system percolate?
    public boolean percolates() {
        if (N == 1) return isOpen(1, 1);
        return grid.connected(N * N + 1, 0);
    }

    // test client (optional)
    public static void main(String[] args) {
    }
}