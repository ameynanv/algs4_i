import java.util.Arrays;

public class Brute {

    public static void main(String[] args) {

        // rescale coordinates and turn on animation mode
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);

        In in = new In(args[0]); // input file
        int N = in.readInt(); // Number of points
        Point[] points = new Point[N];
        for (int i = 0; i < N; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
            points[i].draw();
        }
        Arrays.sort(points);
        for (int p = 0; p < N; p++) {
            for (int q = p; q < N; q++) {
                for (int r = q; r < N; r++) {
                    for (int s = r; s < N; s++) {
                        if (p == q || q == r || r == s
                                || (p == q && q == r && r == s)
                                || (p == r && q == s))
                            continue;
                        double firstSlope = points[p].slopeTo(points[q]);
                        double secondSlope = points[p].slopeTo(points[r]);
                        double thirdSlope = points[p].slopeTo(points[s]);
                        if (firstSlope == secondSlope
                                && secondSlope == thirdSlope) {
                            System.out.println(points[p] + " -> " + points[q]
                                    + " -> " + points[r] + " -> " + points[s]);
                            points[p].drawTo(points[s]);
                        }
                    }
                }
            }
        }

        // display to screen all at once
        StdDraw.show(0);

        // reset the pen radius
        StdDraw.setPenRadius();

    }

}
