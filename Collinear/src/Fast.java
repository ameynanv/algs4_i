import java.util.ArrayList;
import java.util.Arrays;

public class Fast {

    private static class Line implements Comparable<Line> {
        private Point p1;
        private Point p2;

        public Line(Point p1, Point p2) {
            this.p1 = p1;
            this.p2 = p2;
        }

        public String toString() {
            return p1.toString() + "=>" + p2.toString();

        }

        @Override
        public int compareTo(Line that) {
            if (p1.compareTo(that.p1) == 0 && p2.compareTo(that.p2) == 0)
                return 0;
            return 1;
        }

    }

    private static boolean checkIfLineExists(ArrayList<Line> list, Line line) {
        boolean contains = false;
        for (int a = 0; a < list.size(); a++) {
            if (list.get(a).compareTo(line) == 0) {
                contains = true;
            }
        }
        return contains;
    }
    
    public static void main(String[] args) {
        
        ArrayList<Line> list = new ArrayList<Line>();

        // rescale coordinates and turn on animation mode
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        StdDraw.setPenRadius(0.005);
        In in = new In(args[0]); // input file
        int N = in.readInt(); // Number of points
        Point[] points = new Point[N];
        Point[] auxPoints = new Point[N];
        for (int i = 0; i < N; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
            points[i].draw();
        }
        Arrays.sort(points);
        StdDraw.setPenRadius(0.005);
        for (int i = 0; i < N - 1; i++) {
            auxPoints = Arrays.copyOf(points, N);

            Arrays.sort(auxPoints, i + 1, auxPoints.length,
                    auxPoints[i].SLOPE_ORDER);

            double prevSlope = auxPoints[i].slopeTo(auxPoints[i + 1]);
            int cnt = 1;
            int startIndex = i + 1;
            for (int k = i + 2; k < N; k++) {
                double s = auxPoints[i].slopeTo(auxPoints[k]);
                if (prevSlope == s) {
                    cnt++;
                } else {
                    if (cnt >= 3) {
                        Line line = new Line(auxPoints[k - 2], auxPoints[k - 1]);
                        boolean contains = checkIfLineExists(list, line);
                        if (!contains) {
                            list.add(line);
                            StringBuilder builder = new StringBuilder();
                            builder.append(auxPoints[i]);
                            for (int j = startIndex; j < k; j++) {
                                builder.append(" -> ");
                                builder.append(auxPoints[j]);
                                
                            }
                            auxPoints[i].drawTo(auxPoints[k - 1]);
                            System.out.println(builder.toString());
                        }
                    }
                    cnt = 1;
                    prevSlope = s;
                    startIndex = k;
                }
            }
            if (cnt >= 3) {
                Line line = new Line(auxPoints[N - 2], auxPoints[N - 1]);
                boolean contains = checkIfLineExists(list, line);
                if (!contains) {
                    list.add(line);
                    StringBuilder builder = new StringBuilder();
                    builder.append(auxPoints[i]);
                    for (int j = startIndex; j < N; j++) {
                        builder.append(" -> ");
                        builder.append(auxPoints[j]);
                    }
                    auxPoints[i].drawTo(auxPoints[N - 1]);
                    System.out.println(builder.toString());
                }
            }
        }
    }
}
